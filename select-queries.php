<h1> Commands for FINDING things </h1>
<?php

  require "rb-mysql.php";
  R::setup("mysql:host=localhost;dbname=store","root", "");


  echo "<h2> Doing a 'select *'</h2>";
  echo "<h3> select * from locations </h3>";
  // ------------------------------------------------
  // Get ALL things from the database_name
  //  (SELECT * from locations)
  // ------------------------------------------------
  $c = R::findAll("locations");
  print_r($c);

  echo "<br>------------------<br>";

  echo "<h2> Finding ONE thing by ID</h2>";
  echo "<h3> select * from locations where id=2 </h3>";
  // ------------------------------------------------
  // Getting ONE thing from the database
  // SELECT * from locations where id=2
  // ------------------------------------------------
  $a = R::load("locations", 2);    // this gets the "CNE - Food Building" location
  echo $a->name;

  echo "<br>------------------<br>";

   echo "<h2> Select + Where </h2>";
   echo "<h3> select * from locations where city='Toronto' </h3>";

   // ------------------------------------------------
   // Adding a WHERE to your sql query
   //  (SELECT * from locations WHERE city = "Toronto")
   // ------------------------------------------------
    $d = R::find("locations", "city = 'Toronto'");

    foreach ($d as $loc) {
      echo $loc->name . "<br>";
      echo $loc->city . "<br>";
      echo "***<br>";
    }


    echo "<h2> Manually entering an SQL statement </h2>";
    echo "<h3> SELECT * from locations WHERE city <> 'Toronto' ORDER BY name DESC</h3>";
    // ------------------------------------------------
    // Sometimes:
    //   - you don't know what the RedBean command is,
    //   - RedBean doesn't have a command to represent the SQL query you want to do

    // In those case, you can just enter normal SQL
    // Example:
    //  (SELECT * from locations WHERE city <> "Toronto" ORDER BY name DESC)
    // ------------------------------------------------

    $e = R::getAll( "SELECT * FROM locations WHERE city <> 'Toronto' ORDER BY name DESC" );

    echo "<br>------------------<br>";

    // $e will be an ARRAY
    print_r($e);

    echo "<br>------------------<br>";

    for($i=0; $i < count($e); $i++) {
      echo $e[$i]["name"] . "<br>";
      echo $e[$i]["city"] . "<br>";
      echo "*** <br>";
    }



?>
