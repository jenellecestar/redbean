<h1> Commands for INSERTING things </h1>
<h2>
  In this example, I insert new rows into the "locations" table
</h2>

<?php

  require "rb-mysql.php";
  R::setup("mysql:host=localhost;dbname=store","root", "");

  // 1. create a new row in the locations table
  $x  = R::dispense("locations");

  // 2. set the column data for the rows
  $x->name = "The Island Donut Shop";
  $x->street = "909 Main Street";
  $x->city = "Vancouver";
  $x->province = "BC";

  // 3. save row to locations table
  R::store($x);

  // OPTIONAL:  prove to yourself that your INSERT worked by
  // printing out all the locations to the screen
  $c = R::findAll("locations");
  foreach ($c as $item) {
    echo $item->name . "," . $item->city . "<br>";
  }



  echo "<h2> Adding a row + Getting ID number </h2>";

  $y = R::dispense("products");

  $y->name = "Pineapple Donut";
  $y->product_desc = "Tasty donuts with pineapple cream";
  $y->price = "10.99";

  // after saving to the database, R::store will return
  // the id number of the newly created row.

  // save this id to a variable
  $m = R::store($y);

  // then use the variable!
  echo "The id of the new donut is: " . $m;


?>
