<?php

  // 1. Put the redbean php file somewhere in your project folder
  //    Then, add it to your .php file using a "require" statement
  require "rb-mysql.php";

  // 2. Update this with your database information:
  // The blanks are:  HOST, Databse NAME, userNAME, PASSWORD
  // R::setup("mysql:host=_______;dbname=______","_____", "_____");
  R::setup("mysql:host=localhost;dbname=store","root", "");


  // 3. Make a SQL query and save results to a variable
  //    In code below, R::find(___, _____) means:
  //      SELECT * from LOCATIONS where id > 4
  $stores  = R::find( "locations", "id > 4");

  // OPTIONAL DEBUGGING nonsense - output all the results to the screen
  echo "<h1>Here are the results of the SQL query:</h1>";
  print_r($stores);
  echo "<br>";


  echo "<h1> Here are the names of the stores in the array: </h1>";
  // 4.  Do something with the results
  //     In code below, I am outputting the store name to the screen
  foreach ($stores as $item) {
    echo "<h2>";
      echo $item["name"] . "<br>";
    echo "</h2>";
  }

  // 5. Disconnect from the database
  R::close();
?>
